extends Node

enum SimulationState {INITIALIZING, RUNNING, READY, PAUSED, RESTARTING, ENDED}

signal godot_is_ready_for_processing
signal vehicle_cmd_msg_js_change
signal update_sensors
signal change_in_sim_state

var _simulation_state: int # SimulationState

const _update_python_frequency = 2
var _update_python_frequency_counter = 0

var _camera_manager: NodePath
var camera_index: int

var brightness_data : Array

func _init():
	_set_state(SimulationState.INITIALIZING)

# Called when the node enters the scene tree for the first time.
func _ready():
	GameEvaluator.connect("finish_game", self, "_on_finish_game_signal")
	_set_state(SimulationState.READY)
	set_up_line_points()

# probably temporary - testing array code
func test_provide_camera_data(data: Array):
	brightness_data = data

# probably temporary - testing array code
func test_get_camera_data() -> Array:
	return brightness_data
	
func set_up_line_points():
	pass
	
func robot_position_update(position : Vector3):
	pass

func is_running() -> bool:
	return _simulation_state == SimulationState.RUNNING

func pause():
	# If simulation is not already running, cannot pause it.
	if _simulation_state != SimulationState.RUNNING:
		return
	
	_set_state(SimulationState.PAUSED)

func start():
	# If simulation is not ready, cannot start it.
	if _simulation_state != SimulationState.READY and _simulation_state != SimulationState.PAUSED:
		return

	_set_state(SimulationState.RUNNING)

func set_cameras(camera_manager_path: NodePath):
	_camera_manager = camera_manager_path

func restart():
	# If simulation is not already running or paused, cannot restart it.
	if _simulation_state != SimulationState.RUNNING and _simulation_state != SimulationState.PAUSED and _simulation_state != SimulationState.ENDED:
		return
	
	_set_state(SimulationState.RESTARTING)
	
	# To set last camera as initial camera, current index is being saved.
	# Index will be used in the "_ready" function of the CameraManager script.
	if _camera_manager != null:
		camera_index = get_node(_camera_manager).current_index
	
	get_tree().reload_current_scene()
	
	_set_state(SimulationState.READY)

func _on_finish_game_signal():
	# If simulation has not already started, cannot end it.
	if _simulation_state != SimulationState.RUNNING and _simulation_state != SimulationState.PAUSED:
		return

	_set_state(SimulationState.ENDED)

func _set_state(state):
	_simulation_state = state
	emit_signal("change_in_sim_state", _simulation_state)

func _physics_process(delta):
	if not is_running():
		return

	# Process python every _update_python_frequency frames
	# Otherwise, do nothing and continue to the next frame
	if _update_python_frequency_counter != _update_python_frequency:
		_update_python_frequency_counter += 1
		return
	_update_python_frequency_counter = 0

	# Notify all sensors for them to update their states to their js msg definitions.
	emit_signal("update_sensors")
	# Notify CommunicationManager to notfy javascript about the new sensor states.
	emit_signal("godot_is_ready_for_processing")

func on_godot_notified_by_javascript():
	emit_signal("vehicle_cmd_msg_js_change")
