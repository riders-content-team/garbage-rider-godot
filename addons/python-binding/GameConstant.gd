extends Node


var delta_time: float = 0.016
var is_web_active: bool = true

func _ready():
	var web_window: JavaScriptObject = JavaScript.get_interface('window')
	if web_window == null:
		is_web_active = false
