extends Node

# "Cameras" node in the scene.
var _cameras: Node

# Sets "Cameras" node
func set_cameras(cameras: Node):
	_cameras = cameras

# Changes active camera
func change_camera():
	if _cameras:
		_cameras.switch_camera()
	else:
		print("Multiple cameras not supported")

func pervious_camera():
	if _cameras:
		_cameras.previous_camera()
	else:
		print("Multiple cameras not supported")
