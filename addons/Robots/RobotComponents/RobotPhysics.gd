extends Node

# clamps to speed and turning
var maximum_speed = 5.0 # maximum allowed total speed
var maximum_angular_velocity = 2.0 * PI # maximum allowed angular velocity

# mass
var mass = 1.0 # mass of the entire robot

# track physical state of the system
var angular_velocity = 0.0 # current angular velocity
var linear_velocity = Vector3(0.0, 0.0, 0.0) # current velocity
var dynamic_friction = false # is dynamic friction (sliding friction) currently being used

# linear velocity parameters
var scale_linear_force = 5.0 # how much force (accel) we can apply
var linear_friction_scale = 100.0 # how strong side friction is (lower means more skidding)

# angular velocity paramters
var scale_angular_force = 100.0 # how fast we apply requested torque

# dynamic friction parameters
var dynamic_friction_scale = 0.4 # how much to scale down friction when in dynamic (sliding) mode
var velocity_transition_to_static_friction = 0.05 # below this speed we always use static friction
var accel_transition_to_dynamic_friction = 40.0 # if we are trying to change velocity faster than this accel we use dynamic friction
var last_velocity = Vector3(0.0, 0.0, 0.0) # helper last velocity - used to determine instantaneous acceleration

func _init():
	pass # Replace with function body.

func update(robot, dt, target_speed, target_angular_velocity):
	# clamps
	target_speed = clamp(target_speed, -maximum_speed, maximum_speed)
	target_angular_velocity = clamp(target_angular_velocity, -maximum_angular_velocity, maximum_angular_velocity)
		
	var friction_scale = determine_friction_scale(dt)
	update_speed(robot, dt, target_speed, friction_scale)
	update_angular_velocity(robot, dt, target_angular_velocity, friction_scale)

func determine_friction_scale(dt):
	# determine our current actual acceleration using current and last velocity
	var linear_acceleration = (linear_velocity - last_velocity)/dt
	
	# when refactoring to godot frame - we also need to change this
	last_velocity.x = linear_velocity.x
	last_velocity.y = linear_velocity.y

	# pick dynamic_friction if we are accelerating quickly and not moving very slowly
	if linear_velocity.length_squared() < velocity_transition_to_static_friction * velocity_transition_to_static_friction:
		dynamic_friction = false
	elif linear_acceleration.length_squared() >= accel_transition_to_dynamic_friction * accel_transition_to_dynamic_friction:
		dynamic_friction = true

	# calculate friction scale - we have less friction in dynamic mode (sliding)
	var friction_scale = dynamic_friction_scale
	if not dynamic_friction:
		friction_scale = 1.0
	
	return friction_scale
		
func update_speed(robot, dt, target_speed, friction_scale):
	# rotate into the original gazebo frame
	# we could eventually remove this and refactor the code here to be godot
	var godot_x_axis = robot.transform.basis.x.normalized() # will NOT be normalized if scale is applied
	var x_axis = Vector3(godot_x_axis.x, -godot_x_axis.z, 0)

	# friction is scaled to mass
	var friction = mass * friction_scale
	
	# destination speed
	var desired_velocity = x_axis * target_speed
	
	# force
	var force = Vector3(0.0, 0.0, 0.0)
	
	# if velocity is small normalizations will blow up
	var bSmallVelocity = linear_velocity.length_squared() < 0.001

	if bSmallVelocity:
		# just use velocity so it scales down as we approach 0
		# TODO - need to tune the scaling for this
		force = linear_velocity * -linear_friction_scale * friction
	else:
		force = linear_velocity.normalized() * -linear_friction_scale * friction

	# if we are trying to speed up don't apply drag friction along the direction of motion
	if force.dot(desired_velocity) < 0: # is friction opposing direction of movement?
		# in this case we should ignore the friction on the xaxis - allow the robot wheel force to work to speed up the robot
		force -= x_axis * force.dot(x_axis) # subtract it

	# drag friction should not change the direction of motion
	# it can only reduce our speed on that axis to 0
	#var potential_new_velocity = linear_velocity + (force/mass) * dt # velocity we would have if we applied the current friction
	#if potential_new_velocity.dot(linear_velocity) < 0: # is new velocity opposite direction of original
		# remove all velocity on the force direction
	#	linear_velocity -= linear_velocity.dot(force.normalized())
		# remove friction force
	#	force = Vector3(0, 0, 0)

	# add in the force of wheels
	var speed_on_axis = linear_velocity.dot(x_axis)
	force += x_axis * sign(target_speed - speed_on_axis) * friction_scale * scale_linear_force # if slipping we get less force

	# apply the force to the robot
	linear_velocity += (force / mass) * dt

	# clamp the final speed to the maximum
	if linear_velocity.length_squared() > maximum_speed * maximum_speed:
		linear_velocity *= maximum_speed / linear_velocity.length()
		
	# rotate back into the godot frame
	# we could refactor this code to be godot frame - then remove this transform and the original basis change at the top
	var godot_velocity = Vector3(linear_velocity.x, 0, -linear_velocity.y)
	robot.move_and_slide(godot_velocity)

func update_angular_velocity(robot, dt, target_angular_velocity, friction_scale):		
	# apply torque for rotation
	var torque = sign(target_angular_velocity - angular_velocity) * friction_scale * scale_angular_force # if slipping we get less torque
	angular_velocity += torque * dt
	angular_velocity = clamp(angular_velocity, -maximum_angular_velocity, maximum_angular_velocity)
	robot.rotate_y(angular_velocity * dt)
