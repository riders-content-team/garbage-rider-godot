## 
## ScoutRider Physics
## 

extends 'res://addons/Robots/RobotComponents/RobotMotionComponent.gd'

const robot_physics_script = preload("RobotPhysics.gd")

var move: float = 0 # movement speed
var turn: float = 0 # rotation speed


var robot_physics = robot_physics_script.new()

func _ready():
	print(self.name)
	if name == 'GarbageRider':
		EngineInputController.robot = self
		


func _physics_process(delta: float):
	if not GameManager.is_running():
		return
	
	# for debugging we can use this simple control instead of the physics model
	# add these 2 lines and comment out the robot_physics.update line below
	#rotate_y(turn * delta)
	#move_and_slide(transform.basis.x * move)

	robot_physics.update(self, delta, move, turn)

func on_vehicle_cmd_msg_js_change():
	move = vehicle_cmd_msg_js.move
	turn = vehicle_cmd_msg_js.turn

