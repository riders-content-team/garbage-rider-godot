## 
## Motion component for robots. Motion parameters, such as engine power and maximum angle per second, are here.
## TIP:
## 1 - Set "speed" for linear velocity and set "target_rotation_speed" for angular velocity.
## 2 - Call "_calculate_accel" function then call "_apply_accel_to_velocity" function.
## 3 - Use "move_and_slide(velocity * 0.016666 / delta, Vector3.UP, true)" and "rotate_y(rotation_velocity)" in "_physics_process" method
## 
extends 'res://addons/Robots/RobotComponents/RobotBaseComponent.gd'

# Constants
export var engine_power: float = 80
var max_angle_per_sec: float = 3.1415926535 * 2

# Robot state properties.
var velocity: Vector3 = Vector3.ZERO
var rotation_velocity: float = 0.0

var speed: float = 0.0
var current_speed = 0
var target_rotation_speed: float = 0.0

var max_accel: float = engine_power/100
var accel: float = 0.0

var max_rotation_accel: float = max_angle_per_sec/8
var rotation_accel: float = 0

signal robot_speed_changed

# Calculates acceleration.
func _calculate_accel():
	# Calculates acceleration and sets "accel".
	var delta: float = engine_power * speed * GameConstant.delta_time - current_speed

	if abs(delta) <= max_accel * GameConstant.delta_time:
		accel = 0
	elif delta > 0:
		accel = max_accel * GameConstant.delta_time
	else:
		accel = -max_accel * GameConstant.delta_time
	
	# Calculates rotation acceleration and sets "rotation_accel".
	var rotation_delta: float = max_angle_per_sec * target_rotation_speed * GameConstant.delta_time - rotation_velocity
	
	if abs(rotation_delta) <= max_rotation_accel * GameConstant.delta_time:
		rotation_accel = 0
	elif rotation_delta > 0:
		rotation_accel = max_rotation_accel * GameConstant.delta_time
	else:
		rotation_accel = -max_rotation_accel * GameConstant.delta_time

# Applies calculated acceleration
func _apply_accel_to_velocity():
	# Applies acceleration to velocity.
	if velocity.length() < (engine_power * GameConstant.delta_time * 10) and speed == 0.0:
		current_speed = 0
	elif accel == 0:
		current_speed = engine_power * speed * GameConstant.delta_time
	else:
		current_speed += accel
	velocity = transform.basis.x * current_speed
	
	# Applies rotation acceleration to rotation velocity
	if rotation_velocity < (max_rotation_accel * GameConstant.delta_time * 10) and target_rotation_speed == 0:
		rotation_velocity = 0
	elif rotation_accel == 0:
		rotation_velocity = max_angle_per_sec * target_rotation_speed * GameConstant.delta_time
	else:
		rotation_velocity += rotation_accel
	
	GameManager.robot_position_update(transform.origin)
