class_name VehicleCommandMsg

class _VehicleCommandMsg:
	
	var steering_force = 0
	var accel_force = 0.0
	var sleep_time = 0
	var fire = false
	var move = 0
	var turn = 0

	func get_steering_force():
		return steering_force

	func get_accel_force():
		return accel_force
