class_name CameraSensorMsg

class _CameraSensorMsg:
	var width: int = 0;
	var height: int = 0;
	var byte_arr: Array = [];
	var name: String = '';
	var is_constants_set: bool = false;
	var byte_arr_size: int = 0;
	
	func init_constants(w: int, h: int):
		width = w
		height = h
		
		byte_arr_size = w * h * 3
		
		is_constants_set = true
	
	func set_byte_arr(hex_str: String):
		if not is_constants_set:
			push_error('Constants not set')
		
		# TODO: This function is not completed.
